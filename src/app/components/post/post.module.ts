import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PostComponent } from './post.component';

@NgModule({
  declarations: [
    PostComponent
  ],
  imports: [
    BrowserModule,
  ],
  exports: [
    PostComponent
  ]
})
export class PostModule {
}
