import { Component, Input } from '@angular/core';
import { PostClass } from '../../classes/post.class';

@Component({
  selector: 'post',
  templateUrl: './post.component.html'
})
export class PostComponent {
  @Input() post:PostClass;
};
