import { Component, Input } from '@angular/core';

@Component({
  selector: 'tab',
  template: `
  <div [hidden]="!isActive">
    <ng-content></ng-content>
  </div>
  `
})
export class TabComponent {
    @Input() title: string;
    @Input() isActive: boolean = false;
};
