import { NgModule } from '@angular/core';
import { SettingsModule } from '../dashboard/settings/settings.module';
import { BrowserModule } from '@angular/platform-browser';
import { TabsComponent } from './tabs.component';
import { TabComponent } from './tab.component';

@NgModule({
  declarations: [
    TabsComponent,
    TabComponent
  ],
  imports: [
    BrowserModule,
    
  ],
  exports: [
    TabsComponent,
    TabComponent
  ]
})
export class TabsModule {
}
