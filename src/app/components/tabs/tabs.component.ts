import { Component, ContentChildren, QueryList, AfterContentInit } from '@angular/core';
import { TabComponent } from './tab.component';

@Component({
  selector: 'tabs',
  templateUrl: './tabs.component.html'
})
export class TabsComponent implements AfterContentInit {
  @ContentChildren(TabComponent) tabs: QueryList<TabComponent>;

  ngAfterContentInit() {
    let activeTabs = this.tabs.filter(tab => tab.isActive);
    if (!activeTabs.length) this.selectTab(this.tabs.first);
  }

  selectTab(tab: TabComponent) {
    this.tabs.toArray().forEach(item => item.isActive = false);
    tab.isActive = true;
  }
};
