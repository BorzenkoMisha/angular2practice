import { Component, Input } from '@angular/core';
import { MenuItemClass } from '../../classes/menu-item.class';


@Component({
  selector: 'my-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent {
    @Input() menuList: MenuItemClass;
};
