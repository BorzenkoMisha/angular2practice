import { Component } from '@angular/core';
import { MenuItemClass } from '../../classes/menu-item.class';
import { MenuService } from '../../services/menu.service';

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  providers: [MenuService]
})
export class DashboardComponent {
  menuList: MenuItemClass[];
  constructor(menuService: MenuService) {
    this.menuList = menuService.getMenuList();
  };
};
