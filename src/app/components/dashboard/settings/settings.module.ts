import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { SettingsComponent } from './settings.component';
import { GeneralFormComponent } from './general-form.component';
import { TabsModule } from '../../tabs/tabs.module';

@NgModule({
  declarations: [
    SettingsComponent,
    GeneralFormComponent
  ],
  imports: [
    BrowserModule,
    TabsModule
  ],
  exports: [
    SettingsComponent,
    GeneralFormComponent
  ]
})
export class SettingsModule {
}
