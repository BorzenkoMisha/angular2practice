import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { UserClass } from '../../../classes/user.class';

@Component({
  selector: 'general-form',
  templateUrl: './general-form.component.html'
})
export class GeneralFormComponent implements OnInit {
  @Input() user: UserClass;
  constructor() {
  }

  ngOnInit() {

  }
}
