import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { UserClass } from '../../../classes/user.class';

@Component({
  selector: 'my-settings',
  templateUrl: './settings.component.html'
})
export class SettingsComponent implements OnInit {
  user: UserClass;
  constructor(userService: UserService) {
    this.user = userService.getUser();
  }

  ngOnInit() {

  }
}
