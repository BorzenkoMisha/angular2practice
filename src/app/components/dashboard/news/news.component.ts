import { Component } from '@angular/core';
import { PostsService } from '../../../services/posts.service'; 
import { PostClass } from '../../../classes/post.class';

@Component({
  selector: 'my-news',
  templateUrl: './news.component.html'
})
export class NewsComponent {
  public posts: PostClass[];
  constructor(postsService: PostsService) {
    this.posts = postsService.getPosts();
  }
}
