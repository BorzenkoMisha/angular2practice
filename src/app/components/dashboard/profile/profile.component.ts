import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { UserClass } from '../../../classes/user.class';

@Component({
  selector: 'my-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {
  @Input() user: UserClass;
  constructor(userService: UserService) {
    this.user = userService.getUser();
  }

  ngOnInit() {

  }
}
