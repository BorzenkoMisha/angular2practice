import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NewsComponent } from './components/dashboard/news/news.component';
import { ProfileComponent } from './components/dashboard/profile/profile.component';
// Modules
import { TabsModule } from './components/tabs/tabs.module';
import { SettingsModule } from './components/dashboard/settings/settings.module';
import { PostModule } from './components/post/post.module';

import { UserService } from './services/user.service';
import { PostsService } from './services/posts.service';

import { routing } from './app.routing';

import { removeNgStyles, createNewHosts } from '@angularclass/hmr';

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    SettingsModule,
    TabsModule,
    PostModule,
    routing
  ],
  declarations: [
    AppComponent,
    DashboardComponent,
    NewsComponent,
    ProfileComponent,
    SidebarComponent
  ],
  providers: [
    UserService,
    PostsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {}
  hmrOnInit(store) {
    console.log('HMR store', store);
  }
  hmrOnDestroy(store) {
    let cmpLocation = this.appRef.components.map(cmp => cmp.location.nativeElement);
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }
  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
