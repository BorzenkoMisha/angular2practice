import { UserClass } from '../classes/user.class';
import { Injectable } from '@angular/core';

@Injectable()
export class UserService {
  user: UserClass = {
    first_name: 'Alex',
    last_name: 'Hunter',
    age: 21,
    role: 1,
    country: 'Ukraine',
    profile_image: '/img/avatar.jpg',
    profile_background: '/img/1.jpg'
  };
  getUser() { return this.user; };
};
