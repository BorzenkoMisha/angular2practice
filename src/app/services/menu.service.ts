import { MenuItemClass } from '../classes/menu-item.class';
import { Injectable } from '@angular/core';

@Injectable()
export class MenuService {
  private menuList: MenuItemClass[] = [
    {
      text: 'News',
      url: 'news'
    },
    {
      text: 'Profile',
      url: 'profile'
    },
    {
      text: 'Messages',
      url: 'messages'
    },
    {
      text: 'Friends',
      url: 'friends'
    },
    {
      text: 'Communities',
      url: 'communities'
    },
    {
      text: 'Settings',
      url: 'settings'
    }
  ];
  getMenuList(): MenuItemClass[] { return this.menuList; };
};
