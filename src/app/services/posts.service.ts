import { PostClass } from '../classes/post.class';
import { Injectable } from '@angular/core';

@Injectable()
export class PostsService {
  private posts: PostClass[] = [
    {
      id: 1,
      created_at: '23/10/2016',
      title: 'Stoke City midfielder shines but which starting player does The Scout think managers should avoid?',
      description: 'The Bonus Points. Stoke City dominated the Bonus Points System (BPS) following their 2-0 victory at the KCOM Stadium.The scorer of both goals, Xherdan Shaqiri (£6.3m), strolled to the maximum bonus points, tallying 56 in the BPS - the highest return in Gameweek 9. The playmaker registered three attempts against Hull City and completed five of seven dribbles in a mesmerising display. Defenders Ryan Shawcross (£4.9m) and Geoff Cameron (£4.4m) earned the remaining bonus points on offer.',
      category: 'football'
    },
    {
      id: 2,
      created_at: '24/10/2016',
      title: 'Chelsea stun United to move into fourth',
      description: 'Chelsea moved up to fourth place in the Premier League table after producing a superb display to beat Manchester United 4-0 and to spoil former manager Jose Mourinhos return to Stamford Bridge. The home side had the perfect start when Pedro latched onto a long pass, took the ball around the advancing David De Gea and stroked it home with only 30 seconds on the clock - the fastest Premier League goal of the season. Zlatan Ibrahimovic headed just over from an Antonio Valencia cross soon afterwards but Chelsea doubled their advantage on 21 minutes, defender Gary Cahill firing in when a loose ball from a corner fell into his path.',
      category: 'football'
    },
    {
      id: 3,
      created_at: '23/10/2016',
      title: 'Stoke City midfielder shines but which starting player does The Scout think managers should avoid?',
      description: 'The Bonus Points. Stoke City dominated the Bonus Points System (BPS) following their 2-0 victory at the KCOM Stadium.The scorer of both goals, Xherdan Shaqiri (£6.3m), strolled to the maximum bonus points, tallying 56 in the BPS - the highest return in Gameweek 9. The playmaker registered three attempts against Hull City and completed five of seven dribbles in a mesmerising display. Defenders Ryan Shawcross (£4.9m) and Geoff Cameron (£4.4m) earned the remaining bonus points on offer.',
      category: 'football'
    },
    {
      id: 4,
      created_at: '24/10/2016',
      title: 'Chelsea stun United to move into fourth',
      description: 'Chelsea moved up to fourth place in the Premier League table after producing a superb display to beat Manchester United 4-0 and to spoil former manager Jose Mourinhos return to Stamford Bridge. The home side had the perfect start when Pedro latched onto a long pass, took the ball around the advancing David De Gea and stroked it home with only 30 seconds on the clock - the fastest Premier League goal of the season. Zlatan Ibrahimovic headed just over from an Antonio Valencia cross soon afterwards but Chelsea doubled their advantage on 21 minutes, defender Gary Cahill firing in when a loose ball from a corner fell into his path.',
      category: 'football'
    },
    {
      id: 5,
      created_at: '23/10/2016',
      title: 'Stoke City midfielder shines but which starting player does The Scout think managers should avoid?',
      description: 'The Bonus Points. Stoke City dominated the Bonus Points System (BPS) following their 2-0 victory at the KCOM Stadium.The scorer of both goals, Xherdan Shaqiri (£6.3m), strolled to the maximum bonus points, tallying 56 in the BPS - the highest return in Gameweek 9. The playmaker registered three attempts against Hull City and completed five of seven dribbles in a mesmerising display. Defenders Ryan Shawcross (£4.9m) and Geoff Cameron (£4.4m) earned the remaining bonus points on offer.',
      category: 'football'
    },
    {
      id: 6,
      created_at: '24/10/2016',
      title: 'Chelsea stun United to move into fourth',
      description: 'Chelsea moved up to fourth place in the Premier League table after producing a superb display to beat Manchester United 4-0 and to spoil former manager Jose Mourinhos return to Stamford Bridge. The home side had the perfect start when Pedro latched onto a long pass, took the ball around the advancing David De Gea and stroked it home with only 30 seconds on the clock - the fastest Premier League goal of the season. Zlatan Ibrahimovic headed just over from an Antonio Valencia cross soon afterwards but Chelsea doubled their advantage on 21 minutes, defender Gary Cahill firing in when a loose ball from a corner fell into his path.',
      category: 'football'
    }
  ];
  getPosts(): PostClass[] { return this.posts; };
};
