export interface UserClass {
    first_name: string;
    last_name: string;
    age: number;
    role: number;
    profile_image?: string;
    profile_background?: string;
    country?: string;
    city?:string;
};
