export interface PostClass {
    id:number,
    created_at: string,
    title: string;
    description?: string;
    category: string;
};
