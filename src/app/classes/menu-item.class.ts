export interface MenuItemClass {
    text: string;
    url: string;
};
